class Temperature
  def initialize(opts)
    opts[:f] ? self.fahrenheit = opts[:f]: self.celsius = opts[:c]
  end

  def ctof(temp)
   (temp * 9 / 5.0) + 32
  end

  def ftoc(temp)
    (temp - 32) * (5.0 / 9)
  end

  def fahrenheit=(temp)
    @temperature = ftoc(temp)
  end

  def celsius=(temp)
    @temperature = temp
  end

  def in_fahrenheit
    ctof(@temperature)
  end

  def in_celsius
    @temperature
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end
end

class Celsius < Temperature
  def initialize(temp)
    self.celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    self.fahrenheit = temp
  end
end