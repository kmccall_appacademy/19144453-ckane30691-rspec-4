class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    entry.is_a?(String) ? @entries[entry] = nil: @entries.merge!(entry)
  end

  def keywords
    @entries.keys.sort!
  end

  def include?(keyword)
    @entries.keys.each do |key| return true if key == keyword end
    false
  end

  def find(keyword)
    result = {}
    @entries.keys.each do |key|
      result.merge!({key => entries[key]}) if key[keyword] end
    result
  end
  
  def printable
    result = []
    @entries.keys.each do |key|
      result << %Q{[#{key}] "#{@entries[key]}"}
    end
    result.sort.join("\n")
  end
end
