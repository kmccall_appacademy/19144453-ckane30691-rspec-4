class Book
  # TODO: your code goes here!
  attr_accessor :title

  def con_art_or_prep?(word)
    cons_arts_preps = [
      "and", 
      "but", 
      "if", 
      "in", 
      "of", 
      "on", 
      "for", 
      "the", 
      "a", 
      "an"
    ]
    cons_arts_preps.include?(word) ? true: false
  end

  def title=(title)
    result = []
    idx = 0
    for word in title.split(" ")
      if con_art_or_prep?(word) == false || idx == 0
        result << word.capitalize
      else
        result << word
      end
      idx += 1
    end
    @title = result.join(" ")
  end
end
